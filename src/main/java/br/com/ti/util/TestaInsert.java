package br.com.ti.util;

import java.util.Calendar;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.ti.model.Funcionario;

public class TestaInsert {

	public static void main(String[] args) {
		
		Funcionario funcionario = new Funcionario();
		funcionario.setNome("Julio");
		funcionario.setEmail("julio100br@gmail.com");
		funcionario.setDataCadastro(Calendar.getInstance());
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("meuPersistenceUnit");
		EntityManager manager = factory.createEntityManager();
		manager.getTransaction().begin();
		manager.persist(funcionario);
		manager.getTransaction().commit();
		
		System.out.println("ID da tarefa: " + funcionario.getId());
		manager.close();

	}
}
