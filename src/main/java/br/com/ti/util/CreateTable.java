package br.com.ti.util;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class CreateTable {

	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("meuPersistenceUnit");
		factory.close();
	}
}
