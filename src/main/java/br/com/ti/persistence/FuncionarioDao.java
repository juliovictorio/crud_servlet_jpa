package br.com.ti.persistence;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import br.com.ti.model.Funcionario;

public class FuncionarioDao {
	private EntityManagerFactory factory;
	

	public FuncionarioDao(EntityManagerFactory factory) {
		this.factory = factory;
	}

	public Funcionario getByID(Long id) {
		EntityManager manager = factory.createEntityManager();
		Funcionario funcionario = manager.find(Funcionario.class, id);
		return funcionario;
	}
	
	public void insert(Funcionario funcionario) {
		EntityManager manager = factory.createEntityManager();
		manager.getTransaction().begin();
		manager.persist(funcionario);
		manager.getTransaction().commit();
		manager.close();
	}
	
	public void update(Funcionario funcionario){
		EntityManager manager = factory.createEntityManager();
		manager.getTransaction().begin();
		manager.merge(funcionario);
		manager.getTransaction().commit();
	}
	
	public void delete(Funcionario funcionario){
		EntityManager manager = factory.createEntityManager();
		manager.getTransaction().begin();
		manager.remove(manager.contains(funcionario) ? funcionario : manager.merge(funcionario));
		manager.getTransaction().commit();
	}
	
	public List<Funcionario> getFuncionariByWhere(){
		EntityManager manager = factory.createEntityManager();
		List<Funcionario> lista = manager
				.createQuery("select f from Funcionario as f where f.nome = 'julio'")
				.getResultList();
		return lista;
	}
	
	public List<Funcionario> getFuncionariByWhereParameter(){
		EntityManager manager = null;
				Query query = manager
				.createQuery("select t from Tarefa as t "+
				"where t.finalizado = :paramFinalizado");
				
				query.setParameter("paramFinalizado", false);
				
				List<Funcionario> lista = query.getResultList();

		return lista;
	}

	public List<Funcionario> getAll() {
		List<Funcionario> lista = null;
		try {
			EntityManager manager = factory.createEntityManager();
			lista = manager
					.createQuery("select f from Funcionario as F")
					.getResultList();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return lista;
	}

}
