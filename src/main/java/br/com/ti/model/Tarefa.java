package br.com.ti.model;

import java.util.Calendar;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="tarefa")
public class Tarefa {
	
	@Id
	@GeneratedValue
	private Long id;
	
	private String nome;
	private String descricao;
	
	@Temporal(TemporalType.DATE)
	@Column(name="data_inicio")
	private Calendar dataInicio;
	@Temporal(TemporalType.DATE)
	@Column(name="data_fim")
	private Calendar dataFim;
	
	@OneToMany(mappedBy = "tarefa")
	private List<FuncionarioTarefa> funcionarioTarefas;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Calendar getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Calendar dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Calendar getDataFim() {
		return dataFim;
	}

	public void setDataFim(Calendar dataFim) {
		this.dataFim = dataFim;
	}

	public List<FuncionarioTarefa> getFuncionarioTarefas() {
		return funcionarioTarefas;
	}

	public void setFuncionarioTarefas(List<FuncionarioTarefa> funcionarioTarefas) {
		this.funcionarioTarefas = funcionarioTarefas;
	}

}
