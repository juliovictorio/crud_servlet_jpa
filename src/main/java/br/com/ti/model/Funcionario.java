package br.com.ti.model;

import java.util.Calendar;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.JoinColumn;

@Entity
@Table(name="funcionario")
public class Funcionario {

	@Id
	@GeneratedValue
	private Long id;
	private String nome;
	private String email;
	@Temporal(TemporalType.DATE)
	@Column(name="data_cadastro")
	private Calendar dataCadastro;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(
			name = "funcionario_departamento", 
			joinColumns = @JoinColumn(name = "funcionadio_id", referencedColumnName = "id"), 
			inverseJoinColumns = @JoinColumn(name = "departamento_id", referencedColumnName = "id")
			)
	private List<Departamento> departamentos;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(
			name = "funcionario_tarefa", 
			joinColumns = @JoinColumn(name = "funcionadio_id", referencedColumnName = "id"), 
			inverseJoinColumns = @JoinColumn(name = "tarefa_id", referencedColumnName = "id")
			)
	private List<FuncionarioTarefa> tarefasFuncionario;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Calendar getDataCadastro() {
		return dataCadastro;
	}
	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}
	public List<Departamento> getDepartamentos() {
		return departamentos;
	}
	public void setDepartamentos(List<Departamento> departamentos) {
		this.departamentos = departamentos;
	}
	public List<FuncionarioTarefa> getTarefasFuncionario() {
		return tarefasFuncionario;
	}
	public void setTarefasFuncionario(List<FuncionarioTarefa> tarefasFuncionario) {
		this.tarefasFuncionario = tarefasFuncionario;
	}
	
	
}
