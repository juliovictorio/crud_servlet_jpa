package br.com.ti.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="funcionario_tarefa")
public class FuncionarioTarefa {
	
	@Id
	@GeneratedValue
	private Long id;
	
    @ManyToOne
    @JoinColumn(name = "funcionario_id")
	private Funcionario funcionario;
	
    @ManyToOne
    @JoinColumn(name = "tarefa_id")
	private Tarefa tarefa;
	
	@Temporal(TemporalType.DATE)
	@Column(name="data_alocacao_funcionario")
	private Calendar dataAlocacao;

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public Tarefa getTarefa() {
		return tarefa;
	}

	public void setTarefa(Tarefa tarefa) {
		this.tarefa = tarefa;
	}

	public Calendar getDataAlocacao() {
		return dataAlocacao;
	}

	public void setDataAlocacao(Calendar dataAlocacao) {
		this.dataAlocacao = dataAlocacao;
	}

}
