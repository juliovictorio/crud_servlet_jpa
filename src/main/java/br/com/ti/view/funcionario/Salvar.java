package br.com.ti.view.funcionario;

import java.util.Calendar;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.ti.model.Funcionario;
import br.com.ti.persistence.FuncionarioDao;
import br.com.ti.view.Acao;

public class Salvar implements Acao{
	private EntityManagerFactory factory;
	private FuncionarioDao funcionarioDao;
	
	public Salvar() {
		factory = Persistence.createEntityManagerFactory("meuPersistenceUnit");
		funcionarioDao = new FuncionarioDao(factory);
	}
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		String nome = request.getParameter("nome");
		String email = request.getParameter("email");
		String id = request.getParameter("id");
		
		Funcionario funcionario = new Funcionario();
		funcionario.setNome(nome);
		funcionario.setEmail(email);
		
		try {
			if (id != null && !("".equals(id))) {
				atualizar(id, factory, funcionario);
			}else{
				funcionario.setDataCadastro(Calendar.getInstance());
				salvar(funcionario, factory);
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return "manter-funcionario?acao=Consultar";
	}

	private void salvar(Funcionario funcionario, EntityManagerFactory factory) {
		new FuncionarioDao(factory).insert(funcionario);
	}

	private void atualizar(String id, EntityManagerFactory factory, Funcionario funcionario) {
		Funcionario funcionarioDB = funcionarioDao.getByID(Long.valueOf(id));
		funcionarioDB.setNome(funcionario.getNome());
		funcionarioDB.setEmail(funcionario.getEmail());
		funcionarioDao.update(funcionarioDB);
	}

}
