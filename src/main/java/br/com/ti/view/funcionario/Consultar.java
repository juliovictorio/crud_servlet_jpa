package br.com.ti.view.funcionario;

import java.util.List;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.ti.model.Funcionario;
import br.com.ti.persistence.FuncionarioDao;
import br.com.ti.view.Acao;

public class Consultar implements Acao{

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		try {
			EntityManagerFactory factory = Persistence.createEntityManagerFactory("meuPersistenceUnit");
			List<Funcionario> funcionarios = new FuncionarioDao(factory).getAll();
			
			request.setAttribute("funcionarios", funcionarios);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "gerenciar_funcionario.jsp";
	}
}
