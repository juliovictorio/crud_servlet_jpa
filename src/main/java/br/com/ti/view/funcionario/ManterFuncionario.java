package br.com.ti.view.funcionario;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.ti.view.Acao;



@WebServlet("/manter-funcionario")
public class ManterFuncionario extends HttpServlet{

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String parametro = request.getParameter("acao");
		String nomeDaClasse = "br.com.ti.view.funcionario."+(parametro == null? "Consultar" : parametro);
		
		try {
			Class<?> classe = Class.forName(nomeDaClasse);
			Acao logica = (Acao) classe.newInstance();
			
			String retorno = logica.execute(request, response);
			String pagina = null; 
			if (retorno != null && retorno.contains(".jsp")) {
				pagina = "WEB-INF/jsp/"+retorno;
			}else{
				pagina = retorno;
			}
			
			request.getRequestDispatcher(pagina).forward(request, response);
		} catch (Exception e) {
			throw new ServletException("Servlet causou uma exception", e);
		}
	}
}
