package br.com.ti.view.funcionario;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import br.com.ti.model.Funcionario;
import br.com.ti.persistence.FuncionarioDao;
import br.com.ti.view.Acao;

public class Excluir implements Acao{

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		String id = request.getParameter("id");

		try {
			EntityManagerFactory factory = Persistence.createEntityManagerFactory("meuPersistenceUnit");
			FuncionarioDao funcionarioDao = new FuncionarioDao(factory);
			Funcionario funcionario = funcionarioDao.getByID(Long.parseLong(id != null ? id:"0"));
			funcionarioDao.delete(funcionario);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		return "manter-funcionario?acao=Consultar";
	}
}
