<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<style type="text/css">
#tabela {
	margin-top: 50px !important;
}
</style>
<script type="text/javascript">
	$(document).ready(function() {
		$('#divCadastrar').hide();

		$('#btn_mostraForm').click(function() {
			mostrarFormCadastro();
		});
	});
	function mostrarFormCadastro(){
		$('#divCadastrar').show();
		$('#btn_mostraForm').hide();
	}
	function editar(nome, email, id) {
		mostrarFormCadastro();
		$('#nome').val(nome);
		$('#email').val(email);
		$('#id').val(id);
	}
</script>
</head>
<body>
	<div class="jumbotron text-center">
		<h1>Gerenciamento de Funcion�rios</h1>
		<p>Espa�o destinado ao gerenciamento de funcion�rio.</p>
	</div>
	<div class="container">
		<div class="col-sm-6 col-md-6" id="btn_mostraForm">
			<h3>Deseja cadastrar um novo funcion�rio?</h3>
			<p>
				<button type="button" class="btn btn-primary">Cadastrar
					novo funcion�rio</button>
			</p>
		</div>
		<div class="col-sm-6 col-md-6" id="divCadastrar">

			<form role="form" action="manter-funcionario" method="POST">
				<input type="hidden" value="Salvar" name="acao" id="acao">
				<input type="hidden" value="" name="id" id="id">
				<div class="form-group">
					<label for="nome">Nome:</label> <input type="text"
						class="form-control" id="nome" name="nome"
						placeholder="Digite seu nome">
				</div>
				<div class="form-group">
					<label for="email">Email:</label> <input type="email"
						class="form-control" id="email" name="email"
						placeholder="Digite seu email">
				</div>
				<input type="submit" class="btn btn-primary"
					value="Salvar Funcion�rio">
			</form>
		</div>
		<div style="clear: both;"></div>

		<c:if test="${not empty funcionarios}">
			<div id="tabela">
				<table class="table">
					<tr>
						<th>Nome</th>
						<th>Email</th>
						<th>Data de cadastro</th>
						<th>A��o</th>
					</tr>
					<c:forEach var="funcionario" items="${funcionarios}">
						<tr>
							<td>${funcionario.nome}</td>
							<td>${funcionario.email}</td>
							<td><fmt:formatDate value="${funcionario.dataCadastro.time}"
									pattern="dd/MM/yyyy" /></td>
							<td>
							<input type="button"  onclick="editar('${funcionario.nome}', '${funcionario.email}', '${funcionario.id}');" value="Editar" 
							class="btn btn-primary"/>
							<a href="manter-funcionario?acao=Excluir&id=${funcionario.id}" class="btn btn-primary">Excluir</a>
							</td>
						</tr>
					</c:forEach>

				</table>
			</div>
		</c:if>
	</div>
</body>
</html>