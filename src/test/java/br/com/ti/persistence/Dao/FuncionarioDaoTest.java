package br.com.ti.persistence.Dao;

import java.util.Calendar;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.BeforeClass;
import org.junit.Test;

import br.com.ti.model.Funcionario;
import br.com.ti.persistence.FuncionarioDao;
//import static junit.framework.Assert.*;
import static org.junit.Assert.*;

public class FuncionarioDaoTest {
	private static EntityManagerFactory factory;
	private static FuncionarioDao funcionarioDao;
	private static Funcionario funcionarioDefault;
	
	@BeforeClass
	public  static void setUp() {
		factory = Persistence.createEntityManagerFactory("meuPersistenceUnit");
		funcionarioDao = new FuncionarioDao(factory);
		funcionarioDefault = getFuncionario();
		funcionarioDao.insert(funcionarioDefault);
	}
	
	private static Funcionario getFuncionario() {
		Funcionario funcionario = new Funcionario();
		funcionario.setDataCadastro(Calendar.getInstance());
		funcionario.setEmail("teste@teste.com");
		funcionario.setNome("Teste "+System.currentTimeMillis());
		return funcionario;
	}

	@Test
	public void insertTest() {
		Funcionario funcionario = getFuncionario();
		funcionarioDao.insert(funcionario);
		assertNotNull(funcionario.getId());
	}
	
	
	@Test
	public void updateTest() {
		Funcionario funcionario = getFuncionario();
		funcionarioDao.insert(funcionario);
		String nome = "teste editado";
		funcionario.setNome(nome);
		funcionarioDao.update(funcionario);
		
		Funcionario funcionarioEditado = funcionarioDao.getByID(funcionario.getId());
		assertEquals(funcionarioEditado.getNome(),funcionario.getNome());
	}
	
	public void deleteTest() {
		Funcionario funcionario = getFuncionario();
		funcionarioDao.insert(funcionario);
		funcionarioDao.delete(funcionario);
		Funcionario funcionarioDeletado = funcionarioDao.getByID(funcionario.getId());
		assertNull(funcionarioDeletado);

	}


}
